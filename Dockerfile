FROM tomcat:jdk8-openjdk
LABEL maintainer="hugo.vanduijn@naturalis.nl"

ADD oai-pmh.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]
